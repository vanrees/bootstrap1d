{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module SimplePoly
  ( Poly(CL)
  , (.*)
  , (*.)
  , getCL
  , constP
  , padToDeg
  , deg
  , eval
  , shiftArg
  , multArg
  , compose
  , composeWithx
  , sampsToPoly
  , diffPoly
  ) where

newtype Poly a =
  CL
    { getCL :: [a]
    }

-- Parenthesis needed because sometimes coefficients are negative,
-- which might be fixable, but also sometimes coefficients are complex.
instance (Num a, Show a) => Show (Poly a) where
  show (CL []) = show 0
  show (CL (p:ps)) = show p ++ concat (zipWith showterm ps [1 ..])
    where
      showterm c n = "+(" ++ show c ++ ")*x^" ++ show n

deg :: Poly a -> Int
deg (CL []) = 0
deg (CL p) = length p - 1

padToDeg :: (Num a) => Int -> Poly a -> Poly a
padToDeg n (CL p) = CL (p ++ take (n - length p + 1) (fromInteger <$> [0,0 ..]))

instance (Num a) => Num (Poly a) where
  (+) = add
  (*) = mult
  fromInteger a = CL [fromInteger a]
  negate (CL p) = CL (map negate p)
  signum = undefined
  abs = undefined

(.*) :: Num a => a -> Poly a -> Poly a
(.*) x p = constP x * p

-- same as *
infixl 7 .*

(*.) :: Num a => Poly a -> a -> Poly a
(*.) p x = constP x * p

-- same as *
infixl 7 *.

-- instance (Num a) => Algebra a a where
instance Functor Poly where
  fmap f p = CL (f <$> getCL p)

-- addOrConcat is different from zipWith (+) in that it `concats' the
-- remainder rather than terminates if one array is shorter than the other.
addOrConcat :: Num a => [a] -> [a] -> [a]
addOrConcat ((!z):zs) ((!w):ws) = (z + w) : addOrConcat zs ws
addOrConcat zs [] = zs
addOrConcat [] zs = zs

add :: (Num a) => Poly a -> Poly a -> Poly a
add (CL ps) (CL qs) = CL $ addOrConcat ps qs

-- this multiplication works for infinite degree 
mult :: (Num a) => Poly a -> Poly a -> Poly a
mult (CL ps) (CL qs) = CL $ crossdiagonalsums [[p * q | !p <- ps] | !q <- qs]

-- crossdiagonalsums for
-- | a11 a12 a13 |
-- | a21 a22 a23 |
-- | a31 a32 a33 |
-- gives
-- [a11, a12 + a21, a13 + a22 + a31, a23 + a32, a33]
-- Note: also works for infinite arrays, both horizontally and/or vertically
crossdiagonalsums :: Num a => [[a]] -> [a]
crossdiagonalsums [] = []
crossdiagonalsums ([]:_) = []
crossdiagonalsums [xs] = xs
crossdiagonalsums (xs:ys:rest) =
  head xs : crossdiagonalsums (addOrConcat (tail xs) ys : rest)

eval :: (Num a) => a -> Poly a -> a
eval x (CL p) = sum $ zipWith (*) p $ iterate (* x) 1

compose :: (Num a) => Poly a -> Poly a -> Poly a
compose (CL p1s) p2 = sum $ zipWith (.*) p1s $ iterate (* p2) 1

-- if q = compose p1 p2 then q(x) = p1(p2(x)), but we also define composeWithx:
-- if q = composeWithx p1 p2 then q(x) = p1(x * p2(x)). This is useful because
-- we can write an implementation that works for infinite degree:
composeWithx :: (Num a) => Poly a -> Poly a -> Poly a
composeWithx p1 p2 = CL $ crossdiagonalsums coeffmat
  where
    coeffmat = getCL <$> zipWith (.*) (getCL p1) (iterate (* p2) 1)

shiftArg :: (Num a) => a -> Poly a -> Poly a
shiftArg y = flip compose (CL [y, 1])

multArg :: (Num a) => a -> Poly a -> Poly a
multArg y = flip compose (CL [0, y])

constP :: a -> Poly a
constP v = CL [v]

diffPoly :: (Num a) => Poly a -> Poly a
diffPoly (CL []) = CL []
diffPoly (CL ps) = CL (zipWith (*) (tail ps) (fromIntegral <$> [1 ..]))

-- Creates a Poly of degree n-1 from n different (arg,val) pairs
sampsToPoly :: (Num a, Fractional a) => [(a, a)] -> Poly a
sampsToPoly [] = CL []
sampsToPoly [(_, val)] = CL [val]
sampsToPoly ((arg, val):samps) =
  let valP = constP val
      argP = constP arg
      x = CL [0, 1]
      newsamps = (\(a, v) -> (a, (v - val) / (a - arg))) <$> samps
   in valP + (x - argP) * sampsToPoly newsamps
