module Functionals where

import BlocksTaylors
import Data.List
import PolyVectorMatrices
import SimplePoly
import qualified Utils as U

import Debug.Trace

-------------------------------------------------------------------------------
-- Universal functions
-------------------------------------------------------------------------------
shiftpvmarg :: Floating a => a -> PolyVectorMatrix a -> PolyVectorMatrix a
shiftpvmarg a pvm = pvm { mat = map (map (map (shiftArg a))) $ mat pvm
                        , pvmpoles = (+) (-a) <$> pvmpoles pvm
                        , pvmcoeff = pvmcoeff pvm * (pvmexpo pvm ** a)
                        }

evalpvm :: Floating a => a -> PolyVectorMatrix a -> [[[a]]]
evalpvm h pvm =
  let denom = product $ (+) h . negate <$> pvmpoles pvm
      prefactor = pvmcoeff pvm * pvmexpo pvm ** h / denom
   in (((\p -> prefactor * eval h p) <$>) <$>) <$> mat pvm

identity :: Floating a => PolyVectorMatrix a -> [a]
identity = contractWith (repeat 1) . evalpvm 0

data SDP a =
  SDP
    { pvms :: [PolyVectorMatrix a]
    , norm :: [a]
    , obj :: [a]
    }
  deriving (Show)

-- computes vec^t . mat . vec with custom sum and mult
dotdot :: ([b] -> b) -> (a -> b -> b) -> [[b]] -> [a] -> b
dotdot mysum mymult mat vec =
  (mysum . zipWith mymult vec) (mysum . zipWith mymult vec <$> mat)

contractWith :: Num a => [a] -> [[[a]]] -> [a]
contractWith = flip $ dotdot listsum listmult
  where
    listsum bs = sum <$> transpose bs
    listmult x = map (* x)

-------------------------------------------------------------------------------
-- Single Correlator
-------------------------------------------------------------------------------
feasibility1corr :: (Eq a, Floating a) => RhoOrder -> Int -> a -> Maybe a -> Maybe a -> a -> SDP a
feasibility1corr nrho nder h1 mhint mhint2 gap =
   SDP
    { pvms = [shiftpvmarg gap pvm1corr] ++ hintpvms
    , norm = identity pvm1corr
    , obj = replicate (length $ identity pvm1corr) 0
    }
  where 
    [pvm1corr] = polyVectorMatrices1corr nrho nder h1
    constpvm h = pvm1corr { mat = [[constP <$> contractWith [1] (evalpvm h pvm1corr)]] }
    hintpvms = case (mhint, mhint2) of
      (Nothing,      Nothing) -> []
      (Just hint,    Nothing) -> [ constpvm hint ]
      (Just hint, Just hint2) -> [ constpvm hint, constpvm hint2 ]
      (Nothing,   Just hint2) -> [ constpvm hint2 ]

maxopecoeff1corr :: (Eq a, Floating a) => RhoOrder -> Int -> a -> a -> a -> SDP a
maxopecoeff1corr nrho nder h1 hint gap =
  let [pvm1corr] = polyVectorMatrices1corr nrho nder h1
   in SDP
        { pvms = [shiftpvmarg gap pvm1corr]
        , norm = contractWith (repeat 1) $ evalpvm hint pvm1corr
        , obj = identity pvm1corr
        }

-------------------------------------------------------------------------------
-- Three Correlators
-------------------------------------------------------------------------------
feasibility3corr ::
     (Eq a, Floating a) => RhoOrder -> Int -> a -> a -> a -> a -> a -> a -> Maybe a -> SDP a
feasibility3corr nrho nder h1 h2 zEpEgap zEpOgap zOpOgap alpha mc112 =
  let [zEpEPVM, zEpOPVM, zOpOPVM] = polyVectorMatrices3corr nrho nder h1 h2
      constv =
        zipWith (+)
          (contractWith [1] (evalpvm h1 zEpOPVM))
          (contractWith [1, alpha] (evalpvm h2 zEpEPVM))
   in case mc112 of 
        Nothing   -> SDP { pvms = [ shiftpvmarg zEpEgap zEpEPVM
                                  , shiftpvmarg zEpOgap zEpOPVM
                                  , shiftpvmarg zOpOgap zOpOPVM
                                  , zEpEPVM { mat = [[constP <$> constv]] }
                                  ]
                         , norm = identity zEpEPVM
                         , obj = replicate (length $ identity zEpEPVM) 0
                         }
        Just c112 -> SDP { pvms = [ shiftpvmarg zEpEgap zEpEPVM
                                  , shiftpvmarg zEpOgap zEpOPVM
                                  , shiftpvmarg zOpOgap zOpOPVM
                                  ]
                         , norm = zipWith (+)
                                    (identity zEpEPVM)
                                    ((* (c112 * c112)) <$> constv) 
                         , obj = replicate (length $ identity zEpEPVM) 0
                         }

extropecoeff3corr ::
     (Eq a, Floating a) => RhoOrder -> Int -> a -> a -> a -> a -> a -> a -> Bool -> SDP a
extropecoeff3corr nrho nder h1 h2 zEpEgap zEpOgap zOpOgap alpha minimizeQ =
  let [zEpEPVM, zEpOPVM, zOpOPVM] = polyVectorMatrices3corr nrho nder h1 h2
      maxnorm = 
        zipWith (+)
            (contractWith [1] $ evalpvm h1 zEpOPVM)
            (contractWith [1, alpha] $ evalpvm h2 zEpEPVM)
   in SDP
        { pvms =
            [ shiftpvmarg zEpEgap zEpEPVM
            , shiftpvmarg zEpOgap zEpOPVM
            , shiftpvmarg zOpOgap zOpOPVM
            ]
        , norm = if minimizeQ then (fmap negate maxnorm) else maxnorm
        , obj = identity zEpEPVM
        }

feasibility3corrgff ::
     (Eq a, Floating a) => RhoOrder -> Int -> a -> a -> a -> a -> a -> a -> SDP a
feasibility3corrgff nrho nder h1 zEpEgap zEpOgap zOpOgap c112 c222 =
  let h2 = 2 * h1
      [zEpEPVM, zEpOPVM, zOpOPVM] = polyVectorMatrices3corr nrho nder h1 h2
      constv =
        zipWith (+)
          (contractWith [c112] (evalpvm h1 zEpOPVM))
          (contractWith [c112, c222] (evalpvm h2 zEpEPVM))
      ----------------------------------------------------------------
      -- GFF
      ----------------------------------------------------------------
      sqrt2 = sqrt 2 `U.withTypeOf` c112
      c112gff = sqrt2
      c222gff = 2 * c112gff
      -- operator x = O box O
      hx = 2 * h1 + 2
      c11x = sqrt $ 2 * h1^2 * (1 + 2 * h1) / (1 + 4 * h1)
      c22x = 2 * c11x
      -- operator y = O^4
      hy = 4 * h1
      c11y = 0 `U.withTypeOf` c112
      c22y = sqrt 6 `U.withTypeOf` c112
      ----------------------------------------------------------------
      constvgff =
        zipWith4 (\k l m n -> k + l + m + n)
          (contractWith [c112gff] (evalpvm h1 zEpOPVM))
          (contractWith [c112gff, c222gff] (evalpvm h2 zEpEPVM))
          (contractWith [c11x, c22x] (evalpvm hx zEpEPVM))
          (contractWith [c11y, c22y] (evalpvm hy zEpEPVM))
   in SDP { pvms = [ shiftpvmarg zEpEgap zEpEPVM
                  , shiftpvmarg zEpOgap zEpOPVM
                  , shiftpvmarg zOpOgap zOpOPVM
                  ]
          , norm = fmap negate $ zipWith (-) constvgff constv
          , obj = zipWith (+) (identity zEpEPVM) constv
          }

-- -- Navigator version
-- feasibility3corr2ops ::
--      (Eq a, Floating a) => RhoOrder -> Int -> a -> a -> a -> a -> a -> a -> a -> a -> SDP a
-- feasibility3corr2ops nrho nder h1 h2 h4 zEpEgap zEpOgap zOpOgap alpha2 alpha4 =
--   let [zEpEPVM, zEpOPVM, zOpOPVM] = polyVectorMatrices3corr nrho nder h1 h2
--       constv2 =
--         zipWith (+)
--           (contractWith [1] (evalpvm h1 zEpOPVM))
--           (contractWith [1, alpha2] (evalpvm h2 zEpEPVM))
--       constv4 = (contractWith [1, alpha4] (evalpvm h4 zEpEPVM))
--       ----------------------------------------------------------------
--       -- GFFermion
--       ----------------------------------------------------------------
--       -- OPEs: 
--       -- GFF four-point function: O x O -> id + X + Y + ...
--       hX h = 2 * h + 1
--       cX h = sqrt (2 * h) 
--       hY h = 2 * h + 3
--       cY h = sqrt $ 2 * h * h * (h+1) * (2 * h + 1) / (12 * h + 9)
--       -- O1 x O2 = Z0p + Z1m + Z3m + ....
--       hZ0p = h1 + h2
--       cZ0p = 1 -- actually, sqrt 1
--       hZ1m = h1 + h2 + 1
--       cZ1m = sqrt $ 2 * h1  * h2 / (h1 + h2)
--       hZ3m = h1 + h2 + 3
--       cZ3m = sqrt $ 2 * h1 * (1 + h1) * (1 + 2 * h1) * h2 * (1 + h2) * (1 + 2 * h2) / (1 + h1 + h2) / (2 + h1 + h2) / (3 + 2 * h1 + 2 * h2) / 3
--       constvgff =
--         zipWith6 (\k l m n p q -> k + l + m + n + p + q)
--           (contractWith [cX h1, 0] (evalpvm (hX h1) zEpEPVM))
--           (contractWith [cY h1, 0] (evalpvm (hY h1) zEpEPVM))
--           (contractWith [0, cX h2] (evalpvm (hX h2) zEpEPVM))
--           (contractWith [cZ0p] (evalpvm hZ0p zEpOPVM))
--           (contractWith [cZ1m] (evalpvm hZ1m zOpOPVM))
--           (contractWith [cZ3m] (evalpvm hZ3m zOpOPVM))
--   in SDP { pvms = [ shiftpvmarg zEpEgap zEpEPVM
--                   , shiftpvmarg zEpOgap zEpOPVM
--                   , shiftpvmarg zOpOgap zOpOPVM
--                   , zEpEPVM { mat = [[constP <$> constv2]] }
--                   , zEpEPVM { mat = [[constP <$> constv4]] }
--                   ]
--          , norm = fmap negate $ constvgff
--          , obj = identity zEpEPVM
--          }

-- pure feasibility version
feasibility3corr2ops ::
     (Eq a, Floating a) => RhoOrder -> Int -> a -> a -> a -> a -> a -> a -> a -> a -> SDP a
feasibility3corr2ops nrho nder h1 h2 h4 zEpEgap zEpOgap zOpOgap alpha2 alpha4 =
  let [zEpEPVM, zEpOPVM, zOpOPVM] = polyVectorMatrices3corr nrho nder h1 h2
      constv2 =
        zipWith (+)
          (contractWith [1] (evalpvm h1 zEpOPVM))
          (contractWith [1, alpha2] (evalpvm h2 zEpEPVM))
      constv4 = (contractWith [1, alpha4] (evalpvm h4 zEpEPVM))
  in SDP { pvms = [ shiftpvmarg zEpEgap zEpEPVM
                  , shiftpvmarg zEpOgap zEpOPVM
                  , shiftpvmarg zOpOgap zOpOPVM
                  , zEpEPVM { mat = [[constP <$> constv2]] }
                  , zEpEPVM { mat = [[constP <$> constv4]] }
                  ]
         , norm = identity zEpEPVM
         , obj = replicate (length $ identity zEpEPVM) 0
         }