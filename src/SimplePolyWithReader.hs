module SimplePolyWithReader
  ( module SimplePoly )
  where

import Control.Applicative
import BabyParser
import SimplePoly

-- It seems that read . show == id but this is not proven. Read is more general
-- and therefore show . read != id (as strings).
instance (Num a, Read a) => Read (Poly a) where
    readsPrec _ = readpoly 

-- Grammar:
--
-- element = coeff | "x"
-- poly = ("+"|"-"|"") term { ("+"|"-") term }
-- term = factorpow { "*" factorpow }
-- factorpow = factor { "^" int }
-- factor = "(" poly ")" | element
--
-- (curly brackets indicate zero or more occurences)
-- 
-- Convoluted examples that should work:
-- "x+x^1-(3*x^3+5)^2^2-4-x*10^2"
-- Some special cases which all work:
-- "-2^2", "(-2)^2", "1e5", "-(-x)", "2^2^2" 

coeff :: Read a => Parser (Poly a)
coeff = constP <$> Parser reads

var :: Num a => Parser (Poly a)
var = insistc 'x' >> return (CL [0,1])

int :: Parser Integer
int = Parser reads

element :: (Num a, Read a) => Parser (Poly a)
element = coeff <|> var

factor :: (Num a, Read a) => Parser (Poly a)
factor = do { insistc '('; b <- poly; insistc ')'; return b } 
         <|> element

factorpow :: (Num a, Read a) => Parser (Poly a)
factorpow = do { a <- factor; rest a }
                 where rest a = do { insistc '^'; b <- int; rest (a^b) } 
                                <|> return a

term :: (Num a, Read a) => Parser (Poly a)
term = do { a <- factorpow; rest a }
            where rest a = do { insistc '*'; b <- factorpow; rest (a*b) }
                           <|> return a

poly :: (Num a, Read a) => Parser (Poly a)
-- Note the order matters: check that "-2^2" must parse to -4.
poly = do { insistc '+'; a <- term; rest a }
       <|>
       do { insistc '-'; a <- term; rest $ negate a }
       <|>
       do { a <- term; rest a }
  where rest a = do { insistc '+'; b <- term; rest (a+b) }
                 <|> do { insistc '-'; b <- term; rest (a-b) }
                 <|> return a

readpoly :: (Num a, Read a) => String -> [(Poly a, String)]
readpoly = parse poly