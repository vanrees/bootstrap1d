{-# LANGUAGE RankNTypes #-}

module BlocksTaylors
  ( RhoOrder
  , BlockTsApprox(BlockTsApprox, polys, poles, coeff, expo)
  , blockTsApprox
  , blockTsApprox00
  , evalbts
    -- for exact evaluation/testing
  , tsblock00
  , tsblock
  ) where

import Data.List
import Data.Maybe
import Numeric.AD
import SimplePolyWithReader
import qualified Utils as U

type RhoOrder = Integer

-- Block(h, rho) = (4rho)^h Hypergeometric2F1() 
--------------------------------------------------------------------------------
-- Introduction
--------------------------------------------------------------------------------
-- Conventions are those of 1905.06905: in <O1 O2 O3 O4> we have:
--
-- block(a b h z) = z^h Hypergeometric2F1[h+a, h+b, 2h, z]
-- 
-- where a = h2 - h1 and b = h3 - h4 and h are the scaling dimensions.
--
-- To get good 'rational' approximations transform to the rho variable:
rho :: (Floating a) => a -> a
rho z = z / (1 + sqrt (1 - z)) ^ 2

-- In terms of this we have
-- 
-- block(a b h rho) = (4rho)^h sum_n c_n(a b h) rho^n
--
-- with coefficients c_n obeying the recursion relation below, which can be
-- obtained from the conformal Casimir. In the 'polynomial' approximation we
-- truncate the series and get:
--
-- block(a b h rho) = coeff * expo(h rho) * numerator(a b h rho) / denominator(h)
-- 
-- with expo(h rho) = (4 rho)^h and with numerator a polynomial in h. (The
-- numerator is also polynomial in rho but this is not used in the code.)
-- The denominator is of the form prod_n (h - pole_n) and therefore represented
-- as a list of poles. An overall coefficient is sometimes factored out of
-- the numerator.
-- 
-- Taylor series expanding around z = 1/2 yields the BlockTaylorApprox
-- structure:
data BlockTsApprox a =
  BlockTsApprox
    { polys :: [Poly a]
    , poles :: [a]
    , coeff :: a
    , expo :: a
    }
  deriving (Show, Read)

-- with coeff and expo and poles as described above. As for polys, it is the
-- vector obtained by:
-- 
-- (d/dz)^n [ expo(h rho(z)) numerator(a b h rho(z)) ] / (expo h rho(z)) / n!
-- 
-- which, by virtue of dividing by expo, is polynomial in h. The 'eval' is then
-- clear:
evalbts :: (Floating a) => a -> BlockTsApprox a -> [a]
evalbts h bts =
  let denom = product $ (+) h . negate <$> poles bts
      prefactor = coeff bts * expo bts ** h / denom
   in (* prefactor) . eval h <$> polys bts

-- Notice that these are the coefficients of the Taylor series expansion,
-- so the n-th z-derivative divided by a factor n!, evaluated at the given h.
--
-- When a = b = 0 there are simplifications and we have special functions 
-- numerator00 and so on.
--------------------------------------------------------------------------------
-- PART 1: Exact values (used as reference only)
--------------------------------------------------------------------------------
hypgrecursion :: (Fractional a) => a -> a -> a -> a -> Integer -> a
hypgrecursion a b c r n =
  let m = fromIntegral n
   in r * (a + m) * (b + m) / (c + m) / (m + 1)

blockrecursion ::
     (Floating a) => a -> a -> a -> a -> a -> a -> a -> Integer -> a
blockrecursion a b h r w1 w2 w3 n = w4
  where
    w4 =
      (r * r * w2 *
       (2 * h * (2 * a + 2 * b + nn - 1) - 4 * a * (b - nn + 2) +
        (nn - 2) * (4 * b + nn - 1)) +
       r * w3 *
       (2 * h * (2 * a + 2 * b - nn + 1) + 4 * a * (b + nn - 1) +
        (nn - 1) * (4 * b - nn + 2)) +
       r * r * r * w1 * (2 * h * (nn - 2) + (nn - 3) * (nn - 2))) /
      nn /
      (nn + 2 * h - 1)
    nn = fromIntegral n

--------------------------------------------------------------------------------
-- List of terms in the series expansion (internal)
--------------------------------------------------------------------------------
hypgtermlist :: (Fractional a) => a -> a -> a -> a -> [a]
hypgtermlist a b c r =
  1 : U.nestmap (\m w -> w * hypgrecursion a b c r m) 1 [0 ..]

blocktermlist00 :: (Floating a) => a -> a -> [a]
blocktermlist00 h z =
  let r = rho z
   in (((4 * r) ** h) *) <$> hypgtermlist (1 / 2) h (h + 1 / 2) (r ^ 2)

blocktermlist :: (Floating a) => a -> a -> a -> a -> [a]
blocktermlist a b h z =
  let r = rho z
   in U.nestmap3
        (\m w1 w2 w3 -> blockrecursion a b h r w1 w2 w3 m)
        0
        0
        ((4 * r) ** h)
        [1 ..]

--------------------------------------------------------------------------------
-- Get the actual value of a block
--------------------------------------------------------------------------------
sumuntilconverged :: (Eq a, Num a) => [a] -> a
sumuntilconverged termlist =
  let partialsums = U.nestmap (+) 0 termlist
   in fst $ fromJust $ find (uncurry (==)) $ (zip <*> tail . tail) partialsums

blockval00 h z = sumuntilconverged $ blocktermlist00 h z

blockval a b h z = sumuntilconverged $ blocktermlist a b h z

-- useful for debugging
-- hypgval a b c r = sumuntilconverged $ hypgtermlist a b c r
-- blockvalapprox00 h z n = sum $ take (fromInteger n) $ blocktermlist00 h z
-- blockvalapprox a b h z n = sum $ take (fromInteger n) $ blocktermlist a b h z
--------------------------------------------------------------------------------
-- Compute z-taylor coefficients at 1/2 using automatic differentiation (Numeric.AD)
--------------------------------------------------------------------------------
tsblock00 :: (Eq a, Floating a) => a -> [a]
tsblock00 h =
  let diffslist = diffs (blockval00 (auto h)) (1 / 2)
   in U.addinversefactorials diffslist

tsblock :: (Eq a, Floating a) => a -> a -> a -> [a]
tsblock a b h =
  let diffslist = diffs (blockval (auto a) (auto b) (auto h)) (1 / 2)
   in U.addinversefactorials diffslist

-- useful for debugging
-- diffsblockapprox00 h n = diffs (\z -> blockvalapprox00 (auto h) z (auto n)) (1/2)
-- diffsblockapprox a b h n = diffs (\z -> blockvalapprox (auto a) (auto b) (auto h) z (auto n)) (1/2)
--------------------------------------------------------------------------------
-- PART 2: 'rational' approximation to the same derivatives
--------------------------------------------------------------------------------
-- the numrecursion functions correspond to the recursion relation that provide
-- the numerators of the coefficients of the rho series
numrecursion ::
     (Floating a) => a -> a -> Integer -> Poly a -> Poly a -> Poly a -> Poly a
numrecursion a b 1 _ _ w3 = w4
  where
    w4 =
      (w3 *
       CL
         [ (2 + 4 * b - nn) * (nn - 1) + 4 * a * (nn + b - 1)
         , 2 + 4 * a + 4 * b - 2 * nn
         ]) *.
      (1 / nn)
    nn = 1
numrecursion a b 2 _ w2 w3 = w4
  where
    w4 =
      (w3 *
       CL
         [ (2 + 4 * b - nn) * (nn - 1) + 4 * a * (nn + b - 1)
         , 2 + 4 * a + 4 * b - 2 * nn
         ] +
       w2 *
       CL
         [ (nn - 2) * ((nn - 2) * (4 * b + nn - 1) - 4 * a * (2 + b - nn))
         , -4 * (a * (6 + 2 * b - 3 * nn) - (nn - 2) * (3 * b + nn - 1))
         , 4 * (2 * a + 2 * b + nn - 1)
         ]) *.
      (1 / nn)
    nn = 2
numrecursion a b n w1 w2 w3 = w4
  where
    w4 =
      (w3 *
       CL
         [ (2 + 4 * b - nn) * (nn - 1) + 4 * a * (nn + b - 1)
         , 2 + 4 * a + 4 * b - 2 * nn
         ] +
       w2 *
       CL
         [ (nn - 2) * ((nn - 2) * (4 * b + nn - 1) - 4 * a * (2 + b - nn))
         , -4 * (a * (6 + 2 * b - 3 * nn) - (nn - 2) * (3 * b + nn - 1))
         , 4 * (2 * a + 2 * b + nn - 1)
         ] +
       w1 *
       CL
         [ (6 - 5 * nn + nn ^ 2) ^ 2
         , 2 * (nn - 3) * (nn - 2) * (3 * nn - 7)
         , 4 * (nn - 2) * (3 * nn - 8)
         , 8 * (nn - 2)
         ]) *.
      (1 / nn)
    nn = fromIntegral n

numrecursion00 :: (Floating a) => Integer -> Poly a -> Poly a
numrecursion00 n w1 = w1 *. ((1 + 2 * nn) / (2 * (nn + 1))) * CL [nn, 1]
  where
    nn = fromIntegral n

-- we must combine the denominators (giving the extra pochhammer factors, which
-- match with 'poles' in the blockTaylorApprox functions) and then transform a
-- series expansion around rho = 0 to a rho-taylor series expansion around
-- the point rho (1/2).
-- (computing the rho diffs with Numeric.AD was about ten times slower)
numeratorts :: (Floating a) => a -> a -> RhoOrder -> [Poly a]
numeratorts a b n = rhotsathf
    -- first we compute coefficients of rho^n,
    -- where each *term* is a polynomial in h:
    -- rhoseriescoeffs :: [Poly a]
  where
    rhoseriescoeffs =
      zipWith
        (*)
        (U.nestmap3 (numrecursion a b) 0 0 1 [1 .. (n + 1)])
        [U.pochhammer (CL [fromIntegral m, 2]) (n - m) | m <- [0 .. n]]
    -- then we take derivatives at rho (1/2):
    -- (note: constP as a polynomial in h)
    rhohf = constP $ rho (1 / 2)
    -- rhodiffathf :: [Poly a]
    rhodiffsathf = eval rhohf <$> iterate diffPoly (CL rhoseriescoeffs)
    -- then we convert to taylor series:
    rhotsathf = zipWith (.*) U.invfacts rhodiffsathf

numeratorts00 :: (Floating a) => RhoOrder -> [Poly a]
numeratorts00 n = rhotsathf
  where
    nhf = n `quot` 2
    -- first we compute coefficients of (rho^2)^n,
    -- where each *term* is a polynomial in h:
    -- rhosquaredseriescoeffs :: [Poly a]
    rhosqseriescoeffs =
      zipWith
        (*)
        (1 : U.nestmap numrecursion00 1 [0 .. nhf])
        [ U.pochhammer (CL [fromIntegral m + 1 / 2, 1]) (nhf - m)
        | m <- [0 .. nhf]
        ]
    -- then we take derivatives at (rho (1/2))^2:
    -- (note: constP as a polynomial in h)
    rhohf = constP $ rho (1 / 2)
    rhosqhf = rhohf ^ 2
    -- rhosqdiffathf :: [Poly a]
    rhosqdiffsathf = eval rhosqhf <$> iterate diffPoly (CL rhosqseriescoeffs)
    -- then we convert to taylor series
    rhosqtsathf = zipWith (.*) U.invfacts rhosqdiffsathf
    -- and finally we compose with the taylor series of rho^2,
    -- to transform rho^2-derivatives to rho-derivatives:
    rhotsathf = U.chaintaylor rhosqtsathf [rhosqhf, 2 * rhohf, 1]

-- rho-taylor series of the prefactor around rho (1/2), then divided by the
-- prefactor so we get a polynomial in h.
prefactorrelts :: (Floating a) => [Poly a]
prefactorrelts =
  1 : U.nestmap (\n p -> (1 / r / fi n) .* p * (h - fi' n + 1)) 1 [1 ..]
  where
    r = rho (1 / 2)
    h = CL [0, 1]
    fi = fromInteger
    fi' = fromInteger

-- z-taylor series of rho around 1/2; constP as a poly in h
rhotozts :: (Floating a) => [Poly a]
rhotozts = constP <$> U.addinversefactorials (diffs rho (1 / 2))

-- get rho-taylor series, then convert to z-taylor series 
tpolys :: (Floating a) => a -> a -> RhoOrder -> [Poly a]
tpolys a b n = U.chaintaylor prefnumts rhotozts
  where
    prefnumts = U.multtaylor prefactorrelts (numeratorts a b n)

tpolys00 :: (Floating a) => RhoOrder -> [Poly a]
tpolys00 n = U.chaintaylor prefnumts rhotozts
  where
    prefnumts = U.multtaylor prefactorrelts (numeratorts00 n)

--------------------------------------------------------------------------------
-- Finally, putting things together into a data structure
--------------------------------------------------------------------------------
blockTsApprox a b n =
  BlockTsApprox
    { polys = tpolys a b n
    , poles = [-fromIntegral x / 2 | x <- [0 .. n - 1]]
    , coeff = 1 / 2 ^ n
    , expo = 4 * rho (1 / 2)
    }

blockTsApprox00 n =
  BlockTsApprox
    { polys = tpolys00 n
    , poles = [-fromIntegral x - 1 / 2 | x <- [0 .. (n `quot` 2) - 1]]
    , coeff = 1
    , expo = 4 * rho (1 / 2)
    }
