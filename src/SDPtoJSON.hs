{-# LANGUAGE OverloadedStrings #-}

module SDPtoJSON where

import Data.Aeson
import Functionals
import PolyVectorMatrices
import SimplePoly

instance (Show a) => ToJSON (Poly a) where
  toJSON (CL xs) = toJSON (show <$> xs)

instance (Show a) => ToJSON (PolyVectorMatrix a) where
  toJSON pvm =
    object
      [ "DampedRational" .=
        object
          [ "base" .= show (pvmexpo pvm)
          , "constant" .= show (pvmcoeff pvm)
          , "poles" .= toJSON (show <$> pvmpoles pvm)
          ]
      , "polynomials" .= mat pvm
      ]

instance (Show a) => ToJSON (SDP a) where
  toJSON sdp =
    object
      [ "objective" .= toJSON (show <$> obj sdp)
      , "normalization" .= toJSON (show <$> norm sdp)
      , "PositiveMatrixWithPrefactorArray" .= pvms sdp
      ]
