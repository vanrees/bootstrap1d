{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE AllowAmbiguousTypes #-}

module Utils where

import SimplePoly

nestmap :: (a -> b -> b) -> b -> [a] -> [b]
nestmap _ _ [] = []
nestmap f x (a:as) =
  let w = f a x
   in w : nestmap f w as

nestmap2 :: (a -> b -> b -> b) -> b -> b -> [a] -> [b]
nestmap2 _ _ _ [] = []
nestmap2 f x y (a:as) =
  let w = f a x y
   in w : nestmap2 f w y as

nestmap3 :: (a -> b -> b -> b -> b) -> b -> b -> b -> [a] -> [b]
nestmap3 _ _ _ _ [] = []
nestmap3 f x y z (a:as) =
  let w = f a x y z
   in z : nestmap3 f y z w as

--------------------------------------------------------------------------------
-- factorials, binomials, etc.
--------------------------------------------------------------------------------
fact :: (Integral a, Num b) => a -> b
fact n = fromIntegral $ product [1 .. n]

facts :: (Num a) => [a]
facts = fromIntegral <$> 1 : nestmap (*) 1 [1 ..]

addinversefactorials :: (Floating a) => [a] -> [a]
addinversefactorials xs = zipWith (/) xs facts

invfacts :: (Floating a) => [a]
invfacts = 1 : nestmap (\x y -> y / fromIntegral x) 1 [1 ..]

pochhammer :: (Num a, Integral b) => a -> b -> a
pochhammer h n = product [h + fromIntegral m | m <- [0 .. (n - 1)]]

binomialstab :: Num a => [[a]]
binomialstab = binomialsrows $ map fromInteger [1,1 ..]
  where
    binomialsrows r = r : binomialsrows (nestmap (+) 0 r)

-- takes two lists, each corresponding to the Taylor series coefficients
-- of a function, so (d/dz)^n f(z) / n! for n = 0,1,2,...
-- and reproduces the Taylor series for the product of the functions.  
multtaylor :: (Num a) => [a] -> [a] -> [a]
multtaylor fs gs = getCL $ CL fs * CL gs

chaintaylor :: (Num a) => [a] -> [a] -> [a]
chaintaylor fs gs = getCL $ composeWithx (CL fs) (CL $ tail gs)

withTypeOf :: a -> a -> a
withTypeOf x _ = x

-- keep only odd-indexed terms in a zero-indexed array
onlyOdds :: [a] -> [a]
onlyOdds (_:y:xs) = y : onlyOdds xs
onlyOdds [_] = []
onlyOdds [] = []

-- keep only even-indexed terms in a zero-indexed array
onlyEvens :: [a] -> [a]
onlyEvens (x:_:xs) = x : onlyEvens xs
onlyEvens [x] = [x]
onlyEvens [] = []

signAlternate :: Num a => [a] -> [a]
signAlternate (x:y:xs) = x : -y : signAlternate xs
signAlternate [x] = [x]
signAlternate [] = []

-- get two matrices and put them in block diagonal form 
toBlockMatrixPair :: Num a => [[a]] -> [[a]] -> [[a]]
toBlockMatrixPair xss [] = xss
toBlockMatrixPair xss yss =
  map (++ zeroestoappend) xss ++ map (zeroestoprepend ++) yss
  where
    zeroestoappend = replicate (length $ head yss) 0
    zeroestoprepend = replicate (length $ head xss) 0

-- get a list of matrices and put them in block diagonal form 
toBlockMatrix :: Num a => [[[a]]] -> [[a]]
toBlockMatrix = foldr toBlockMatrixPair []

-- get a list of vectors and put them in block diagonal form
vectorsToBlockMatrix :: Num a => [[a]] -> [[a]]
vectorsToBlockMatrix xs = toBlockMatrix $ map (: []) xs
