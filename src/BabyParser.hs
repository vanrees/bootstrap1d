{-# LANGUAGE LambdaCase #-}

module BabyParser where

import Control.Applicative

newtype Parser a = Parser { parse :: String -> [(a,String)] }

instance Functor Parser where
    -- fmap :: (a -> b) -> Parser a -> Parser b
    fmap f pa = Parser (\s -> [ (f as, rs) | (as, rs) <- parse pa s])

instance Applicative Parser where
    -- pure ::   a -> Parser a
    pure a = Parser (\s -> [(a,s)])
    -- (<*>) :: Parser (a -> b) -> Parser a -> Parser b 
    (<*>) pfab pa = 
        Parser (\s ->  [ (f a, rems') | (f, rems) <- parse pfab s
                                      , (a, rems') <- parse pa rems ] )

instance Monad Parser where
    -- return :: a -> Parser a
    return a = Parser (\s -> [(a,s)])
    -- (>>=) :: Parser a -> (a -> Parser b) -> Parser b
    (>>=) pa fapb =
        Parser (\s -> concat [parse (fapb a) rems | (a, rems) <- parse pa s])

instance Alternative Parser where
    -- empty :: Parser a
    empty = Parser (const [])
    -- (<|>) :: Parser a -> Parser a -> Parser a
    (<|>) pa1 pa2 = Parser (\s -> case parse pa1 s of 
                                    []  -> parse pa2 s
                                    res -> res) 

item :: Parser Char
item = Parser (\case "" -> []
                     (c:cs) -> [(c,cs)])


insistc :: Char -> Parser Char
insistc x = item >>= (\c -> if c==x then return c else empty)

insistnotc :: Char -> Parser Char
insistnotc x = item >>= (\c -> if c/=x then return c else empty)

insist :: String -> Parser String
insist "" = return ""
insist (c:cs) = do { insistc c; insist cs; return (c:cs) } 
insistcin :: String -> Parser Char
insistcin cs = item >>= (\c -> if c `elem` cs then return c else empty)

removespaces :: String -> String
removespaces s = case parse nospaces s of
                [(cs,"")] -> cs
                _         -> error "How could I not remove spaces???" 
                where 
                  -- nospaces :: Parser String
                  nospaces = many $ do {many $ insistc ' '; item}

removewhitespace :: String -> String
removewhitespace s = case parse eatwhites s of
                     [(cs,"")] -> cs
                     _         -> error "How could I not remove whitespace???"
                     where
                        eatwhites = many $ do { many $ insistcin " \t\n"; item }

-- behavior:
-- list begins with 'beg' (can be empty),
-- and ends with 'end' (can be empty), 
-- and items must be separated by 'sep'.
-- If p eats (part of) 'sep' or (part of) 'end' then you're in trouble.
readcustomlist :: String -> String -> String -> Parser a -> Parser [a]
readcustomlist _ "" _ _ = error "List separator cannot be empty." 
readcustomlist beg sep end p = do { insist beg; rems [] <|> emptylist } 
  where rems xs   = do { a <- p; insist sep; rems (xs++[a]) }
                    <|> 
                    do { a <- p; insist end; return (xs++[a]) }
        emptylist = do { insist end; return [] }
