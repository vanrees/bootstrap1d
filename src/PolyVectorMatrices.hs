module PolyVectorMatrices
  ( PolyVectorMatrix(PVM)
  , prefts
  , mat
  , pvmpoles
  , pvmexpo
  , pvmcoeff
  , polyVectorMatrices1corr
  , polyVectorMatrices3corr
  ) where

import BlocksTaylors

import Data.Maybe ()
import Numeric.AD
import SimplePoly
import qualified Utils as U

-------------------------------------------------------------------------------
-- Universal functions
-------------------------------------------------------------------------------
-- Taylor series of (1-z)^h at z = 1/2,  constant as Poly in (another) h
prefts :: (Floating a) => a -> [Poly a]
prefts h =
  constP <$> U.addinversefactorials (diffs (\x -> (1 - x) ** auto h) (1 / 2))

-- auxiliary function to udate 'polys' field of bts with f
appliedToPolysOf :: ([Poly a] -> [Poly a]) -> BlockTsApprox a -> BlockTsApprox a
appliedToPolysOf f bts = bts {polys = f (polys bts)}

data PolyVectorMatrix a =
  PVM
    { mat :: [[[Poly a]]]
    , pvmpoles :: [a]
    , pvmexpo :: a
    , pvmcoeff :: a
    }
  deriving (Show)

-- Contribution of a single crossing equation to a PolyVectorMatrix a
type PolyVectorMatrixContr a = [[Maybe (BlockTsApprox a)]]

-- take n polynomials from the BlockTsApprox inside a PolyVectorMatrixContr
-- and add them to the PVM; checks also whether the prefactors match.
mergeSingleContr ::
     (Num a, Eq a)
  => Int
  -> PolyVectorMatrix a
  -> PolyVectorMatrixContr a
  -> PolyVectorMatrix a
mergeSingleContr n pvm pvmcontr =
  pvm {mat = zipWith (zipWith concatels) (mat pvm) pvmcontr}
  where
    concatels pvmel Nothing = pvmel ++ replicate n 0
    concatels pvmel (Just cbta) =
      if verifyprefactor cbta pvm
        then pvmel ++ take n (polys cbta)
        else error "Prefactors do not match."
    verifyprefactor cbta pvm =
      poles cbta == pvmpoles pvm &&
      expo cbta == pvmexpo pvm && coeff cbta == pvmcoeff pvm

mergeContr ::
     (Num a, Eq a)
  => Int
  -> [PolyVectorMatrix a]
  -> [PolyVectorMatrixContr a]
  -> [PolyVectorMatrix a]
mergeContr n = zipWith (mergeSingleContr n)

-------------------------------------------------------------------------------
-- Single Correlator
-------------------------------------------------------------------------------
ders1corr :: Floating a => RhoOrder -> a -> [PolyVectorMatrixContr a]
ders1corr nrho h1 = [[[Just contrs]]]
  where
    contrs = convolver `appliedToPolysOf` blockTsApprox00 nrho
    convolver = fmap (*. 2) . U.onlyOdds . U.multtaylor (prefts (2 * h1))

template1corr :: Floating a => RhoOrder -> [PolyVectorMatrix a]
template1corr nrho =
  [ PVM
      { mat = [[[]]]
      , pvmpoles = poles $ blockTsApprox00 nrho
      , pvmexpo = expo $ blockTsApprox00 nrho
      , pvmcoeff = coeff $ blockTsApprox00 nrho
      }
  ]

polyVectorMatrices1corr ::
     (Eq a, Floating a) => RhoOrder -> Int -> a -> [PolyVectorMatrix a]
polyVectorMatrices1corr nrho nder h1 =
  mergeContr nder (template1corr nrho) (ders1corr nrho h1)

-------------------------------------------------------------------------------
-- Three correlators with Z2 symmetry
-------------------------------------------------------------------------------
-- to avoid recomputing these (or perhaps reloading them in a future version), 
-- we store them in a data structure which we pass around when needed. 
data AllBlockTs a =
  AllBlockTs
    { blockts1111 :: BlockTsApprox a
    , blockts2222 :: BlockTsApprox a
    , blockts1122 :: BlockTsApprox a
    , blockts1221 :: BlockTsApprox a
    , blockts1212 :: BlockTsApprox a
    }

allblockts :: Floating a => RhoOrder -> a -> a -> AllBlockTs a
allblockts n h1 h2 =
  AllBlockTs
    { blockts1111 = blockTs00
    , blockts2222 = blockTs00
    , blockts1122 = blockTs00
    , blockts1221 = blockTsApprox (h2 - h1) (h2 - h1) n
    , blockts1212 = blockTsApprox (h1 - h2) (h2 - h1) n
    }
  where
    blockTs00 = blockTsApprox00 n

eqn1ders :: Floating a => AllBlockTs a -> a -> a -> [PolyVectorMatrixContr a]
eqn1ders allbts h1 _ = [zEpEcontr, zEpOcontr, zOpOcontr]
  where
    zEpEcontr = [[Just contrs, Nothing], [Nothing, Nothing]]
    zEpOcontr = [[Nothing]]
    zOpOcontr = [[Nothing]]
    contrs = convolver `appliedToPolysOf` blockts1111 allbts
    convolver = fmap (*. 2) . U.onlyOdds . U.multtaylor (prefts (2 * h1))

eqn2ders :: Floating a => AllBlockTs a -> a -> a -> [PolyVectorMatrixContr a]
eqn2ders allbts _ h2 = [zEpEcontr, zEpOcontr, zOpOcontr]
  where
    zEpEcontr = [[Nothing, Nothing], [Nothing, Just contrs]]
    zEpOcontr = [[Nothing]]
    zOpOcontr = [[Nothing]]
    contrs = convolver `appliedToPolysOf` blockts2222 allbts
    convolver = fmap (*. 2) . U.onlyOdds . U.multtaylor (prefts (2 * h2))

eqn5ders :: Floating a => AllBlockTs a -> a -> a -> [PolyVectorMatrixContr a]
eqn5ders allbts h1 h2 = [zEpEcontr, zEpOcontr, zOpOcontr]
  where
    zEpEcontr = [[Nothing, Nothing], [Nothing, Nothing]]
    zEpOcontr = [[Just contrs]]
    zOpOcontr = [[Just $ fmap negate `appliedToPolysOf` contrs]]
    contrs = convolver `appliedToPolysOf` blockts1212 allbts
    convolver = fmap (*. 2) . U.onlyOdds . U.multtaylor (prefts (h1 + h2))

eqn6ders :: Floating a => AllBlockTs a -> a -> a -> [PolyVectorMatrixContr a]
eqn6ders allbts h1 h2 = [zEpEcontr, zEpOcontr, zOpOcontr]
  where
    zEpEcontr = [[Nothing, Just contrsLHS], [Just contrsLHS, Nothing]]
    zEpOcontr = [[Just contrsRHS]]
    zOpOcontr = [[Just contrsRHS]]
    contrsLHS = convolverLHS `appliedToPolysOf` blockts1122 allbts
    convolverLHS =
      fmap (*. (1 / 2)) . U.multtaylor (prefts (h1 + h2))
    contrsRHS = convolverRHS `appliedToPolysOf` blockts1221 allbts
    convolverRHS =
      fmap (*. (-1)) . U.signAlternate . U.multtaylor (prefts (2 * h2))

template3corr :: Floating a => AllBlockTs a -> [PolyVectorMatrix a]
template3corr allbts = [zEpE, zEpO, zOpO]
  where
    zEpE =
      PVM
        { mat = [[[], []], [[], []]]
        , pvmpoles = poles $ blockts1111 allbts
        , pvmexpo = expo $ blockts1111 allbts
        , pvmcoeff = coeff $ blockts1111 allbts
        }
    zEpO =
      PVM
        { mat = [[[]]]
        , pvmpoles = poles $ blockts1221 allbts
        , pvmexpo = expo $ blockts1221 allbts
        , pvmcoeff = coeff $ blockts1221 allbts
        }
    zOpO =
      PVM
        { mat = [[[]]]
        , pvmpoles = poles $ blockts1221 allbts
        , pvmexpo = expo $ blockts1221 allbts
        , pvmcoeff = coeff $ blockts1221 allbts
        }

polyVectorMatrices3corr ::
     (Eq a, Floating a) => RhoOrder -> Int -> a -> a -> [PolyVectorMatrix a]
polyVectorMatrices3corr nrho nder h1 h2 =
  foldl
    mergeContrs3
    (template3corr allbts)
    [ nder `from` eqn1ders allbts h1 h2
    , nder `from` eqn2ders allbts h1 h2
    , nder `from` eqn5ders allbts h1 h2
    , (2 * nder) `from` eqn6ders allbts h1 h2
    ]
  where
    allbts = allblockts nrho h1 h2
    from a b = (a, b)
    mergeContrs3 pvms (n, pvmcontrs) = mergeContr n pvms pvmcontrs
    -- sets the initial sizes and prefactors.
