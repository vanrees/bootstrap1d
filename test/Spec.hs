{-# LANGUAGE DataKinds #-}

module Main where

import BlocksTaylors
import Data.Typeable
import Numeric.Rounded
import SimplePolyWithReader
import Text.Printf
import qualified Utils as U

type PFloat = Rounded 'TowardZero 256

polyReaderTest ::
     (Num a, Typeable a, Read a, Show a) => (String, Poly a) -> IO ()
polyReaderTest (pstr, ans) = do
  putStrLn $ "Parsing \"" ++ pstr ++ "\" as " ++ show (typeOf ans)
  putStrLn $ "Read:   " ++ show (read pstr `U.withTypeOf` ans)
  putStrLn $ "Answer: " ++ show ans
  putStrLn ""

-- TODO check against MMA
blockApproxTest :: PFloat -> PFloat -> PFloat -> RhoOrder -> Int -> IO ()
blockApproxTest a b h nmax deriv = do
  mapM_ showblockapprox nlist
  putStrLn $ "Exact value:           " ++ show exactval
  putStrLn ""
  where
    nlist = [0] ++ ((* (nmax `quot` 5)) <$> [1, 2, 3, 4]) ++ [nmax]
    blockapprox n = evalbts h (blockTsApprox a b n) !! deriv
    exactval = tsblock a b h !! deriv
    showblockapprox n =
      putStrLn $
      "Approx (rhoOrder " ++ printf "%03d" n ++ "): " ++ show (blockapprox n)

-- TODO check against MMA
blockApproxTest00 :: PFloat -> RhoOrder -> Int -> IO ()
blockApproxTest00 h nmax deriv = do
  mapM_ showblockapprox nlist
  putStrLn $ "Exact value:           " ++ show exactval
  putStrLn ""
  where
    nlist = [0] ++ ((* (nmax `quot` 5)) <$> [1, 2, 3, 4]) ++ [nmax]
    blockapprox n = evalbts h (blockTsApprox00 n) !! deriv
    exactval = tsblock00 h !! deriv
    showblockapprox n =
      putStrLn $
      "Approx (rhoOrder " ++ printf "%03d" n ++ "): " ++ show (blockapprox n)

main :: IO ()
main = do
  mapM_
    polyReaderTest
    ([ ("(3*x+5+4)*(-2^2^2*x)", CL [0, -16] * CL [9, 3])
     , ("-2^2", CL [-4])
     , ("(-2)^2", CL [4])
     , ("-(-x^3)-3", CL [-3, 0, 0, 1])
     ] :: [(String, Poly Integer)])
  mapM_ polyReaderTest [("1e-3", CL [1 / 1000] :: Poly PFloat)]
  putStrLn "Part of an infinite degree \"polynomial\": "
  putStrLn $ take 80 $ show (CL [1 ..] :: Poly Integer)
  putStrLn ""
  blockApproxTest 0.1 0.2 3 50 5
  blockApproxTest00 72 50 10
