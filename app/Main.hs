{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE RecordWildCards #-}

module Main where

import BlocksTaylors (RhoOrder)
import Data.Aeson
import Data.Data
import Data.Maybe
import Functionals
import Numeric.Rounded
import SDPtoJSON ()
import System.Console.CmdArgs

type PFloat = (Rounded 'TowardZero 512)

-- adding a new option is easy: just specify the arguments lists thrice below.
data ProblemClass
  = Feasibility1Corr
      { nrho :: Maybe RhoOrder
      , nder :: Maybe Int
      , h1 :: Maybe String
      , hint :: Maybe String
      , gap :: Maybe String
      , filename :: Maybe String
      }
  | MaxOPECoeff1Corr
      { nrho :: Maybe RhoOrder
      , nder :: Maybe Int
      , h1 :: Maybe String
      , hint :: Maybe String
      , gap :: Maybe String
      , filename :: Maybe String
      }
  | Feasibility3Corr
      { nrho :: Maybe RhoOrder
      , nder :: Maybe Int
      , h1 :: Maybe String
      , h2 :: Maybe String
      , zEpEgap :: Maybe String
      , zEpOgap :: Maybe String
      , zOpOgap :: Maybe String
      , alpha :: Maybe String
      , c112 :: Maybe String
      , filename :: Maybe String
      }
   | ExtrOPECoeff3Corr
      { nrho :: Maybe RhoOrder
      , nder :: Maybe Int
      , h1 :: Maybe String
      , h2 :: Maybe String
      , zEpEgap :: Maybe String
      , zEpOgap :: Maybe String
      , zOpOgap :: Maybe String
      , alpha :: Maybe String
      , minimize :: Bool
      , filename :: Maybe String
      }
    | Feasibility3CorrGFF
      { nrho :: Maybe RhoOrder
      , nder :: Maybe Int
      , h1 :: Maybe String
      , zEpEgap :: Maybe String
      , zEpOgap :: Maybe String
      , zOpOgap :: Maybe String
      , c112 :: Maybe String
      , c222 :: Maybe String
      , filename :: Maybe String
      }
    | Feasibility3Corr2Ops
      { nrho :: Maybe RhoOrder
      , nder :: Maybe Int
      , h1 :: Maybe String
      , h2 :: Maybe String
      , h4 :: Maybe String
      , zEpEgap :: Maybe String
      , zEpOgap :: Maybe String
      , zOpOgap :: Maybe String
      , alpha2 :: Maybe String
      , alpha4 :: Maybe String
      , filename :: Maybe String
      }
  deriving (Show, Data, Typeable)

feasibility1CorrArg :: ProblemClass
feasibility1CorrArg =
  Feasibility1Corr {nrho = def, nder = def, h1 = def, hint = def, gap = def, filename = def}

maxOPECCoeff1CorrArg :: ProblemClass
maxOPECCoeff1CorrArg =
  MaxOPECoeff1Corr
    {nrho = def, nder = def, h1 = def, hint = def, gap = def, filename = def}

feasibility3CorrArg :: ProblemClass
feasibility3CorrArg =
  Feasibility3Corr
    { nrho = def
    , nder = def
    , h1 = def
    , h2 = def
    , zEpEgap = def
    , zEpOgap = def
    , zOpOgap = def
    , alpha = def
    , c112 = def
    , filename = def
    }

extrOPECoeff3CorrArg :: ProblemClass
extrOPECoeff3CorrArg =
  ExtrOPECoeff3Corr
    { nrho = def
    , nder = def
    , h1 = def
    , h2 = def
    , zEpEgap = def
    , zEpOgap = def
    , zOpOgap = def
    , alpha = def
    , minimize = def -- defaults to false
    , filename = def
    }

feasibility3CorrGFFArg :: ProblemClass
feasibility3CorrGFFArg =
  Feasibility3CorrGFF
    { nrho = def
    , nder = def
    , h1 = def
    , zEpEgap = def
    , zEpOgap = def
    , zOpOgap = def
    , c112 = def
    , c222 = def
    , filename = def
    }

feasibility3Corr2OpsArg :: ProblemClass
feasibility3Corr2OpsArg =
  Feasibility3Corr2Ops
    { nrho = def
    , nder = def
    , h1 = def
    , h2 = def
    , h4 = def
    , zEpEgap = def
    , zEpOgap = def
    , zOpOgap = def
    , alpha2 = def
    , alpha4 = def
    , filename = def
    }

main = do
  problem <-
    cmdArgs
      (modes [ feasibility1CorrArg
             , maxOPECCoeff1CorrArg
             , feasibility3CorrArg
             , extrOPECoeff3CorrArg
             , feasibility3CorrGFFArg
             , feasibility3Corr2OpsArg ])
  encodeFile (fromJust $ filename problem) $
  -- unsafe: verify the correct order of the arguments below!
    case problem of
      Feasibility1Corr {..} ->
        feasibility1corr
          (fromJust nrho)
          (fromJust nder)
          (read $ fromJust h1 :: PFloat)
          (read <$> hint :: Maybe PFloat)
          (read $ fromJust gap :: PFloat)
      MaxOPECoeff1Corr {..} ->
        maxopecoeff1corr
          (fromJust nrho)
          (fromJust nder)
          (read $ fromJust h1 :: PFloat)
          (read $ fromJust hint :: PFloat)
          (read $ fromJust gap :: PFloat)
      Feasibility3Corr {..} ->
        feasibility3corr
          (fromJust nrho)
          (fromJust nder)
          (read $ fromJust h1 :: PFloat)
          (read $ fromJust h2 :: PFloat)
          (read $ fromJust zEpEgap :: PFloat)
          (read $ fromJust zEpOgap :: PFloat)
          (read $ fromJust zOpOgap :: PFloat)
          (read $ fromJust alpha :: PFloat)
          (read <$> c112 :: Maybe PFloat)
      ExtrOPECoeff3Corr {..} ->
        extropecoeff3corr
          (fromJust nrho)
          (fromJust nder)
          (read $ fromJust h1 :: PFloat)
          (read $ fromJust h2 :: PFloat)
          (read $ fromJust zEpEgap :: PFloat)
          (read $ fromJust zEpOgap :: PFloat)
          (read $ fromJust zOpOgap :: PFloat)
          (read $ fromJust alpha :: PFloat)
          (minimize)
      Feasibility3CorrGFF {..} ->
        feasibility3corrgff
          (fromJust nrho)
          (fromJust nder)
          (read $ fromJust h1 :: PFloat)
          (read $ fromJust zEpEgap :: PFloat)
          (read $ fromJust zEpOgap :: PFloat)
          (read $ fromJust zOpOgap :: PFloat)
          (read $ fromJust c112 :: PFloat)
          (read $ fromJust c222 :: PFloat)
      Feasibility3Corr2Ops {..} ->
        feasibility3corr2ops
          (fromJust nrho)
          (fromJust nder)
          (read $ fromJust h1 :: PFloat)
          (read $ fromJust h2 :: PFloat)
          (read $ fromJust h4 :: PFloat)
          (read $ fromJust zEpEgap :: PFloat)
          (read $ fromJust zEpOgap :: PFloat)
          (read $ fromJust zOpOgap :: PFloat)
          (read $ fromJust alpha2 :: PFloat)
          (read $ fromJust alpha4 :: PFloat)